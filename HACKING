# Tagging

Example tagging v5:

    $ (export T=v5; git tag -sam $T $T && git push && git push --tags)

# Generating and verifying detached signatures for stboot images

Example using Linus' keys.

## downloading released images, from the package repo
click https://git.glasklar.is/system-transparency/project/qa-images/-/packages and select the qa-stboot version you want to publish

## downloading images from artifacts
click https://git.glasklar.is/system-transparency/project/qa-images/-/pipelines
unzip ~/Downloads/artifacts.zip
cd stimages/build
mv stboot.uki qa-stboot-amd64.uki
mv stboot.iso qa-stboot-amd64.iso

## generating allowed_signers file and signing two files twice
echo 'linus@glasklarteknik.se,qa@system-transparency.org namespaces="qa@system-transparency.org" sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIMdhCI0ClJsD4cTCuU2CPLiYf1KYk+XN3BC5pKQstYINAAAABHNzaDo= linus:YK5C-31' > allowed_signers
ssh-keygen -Y sign -f ~/.ssh/linus:YK5C-31 -n qa@system-transparency.org qa-stboot-amd64.{uki,iso}
gpg --detach-sign --armor --local-user 8C4CD511095E982EB0EFBFA21E8BF34923291265 qa-stboot-amd64.uki
gpg --detach-sign --armor --local-user 8C4CD511095E982EB0EFBFA21E8BF34923291265 qa-stboot-amd64.iso

## uploading seven files
for f in allowed_signers qa-stboot-amd64.{uki,iso} qa-stboot-amd64.{uki,iso}.sig qa-stboot-amd64.{uki,iso}.asc; do
  [ -f $f ] && echo "$f" | cat - "$f" | ssh -l st listen.sigsum.org; done

## downloading seven files and verifying two of them, twice
for f in allowed_signers qa-stboot-amd64.{uki,iso} qa-stboot-amd64.{uki,iso}.sig qa-stboot-amd64.{uki,iso}.asc; do
  curl -sO https://st.glasklar.is/st/qa/"$f"; done
for f in qa-stboot-amd64.{uki,iso}; do
  ssh-keygen -Y verify -f allowed_signers -I qa@system-transparency.org -n qa@system-transparency.org -s "$f".sig < "$f" || echo "$f: verification failed"
  gpg --verify "$f".asc "$f"
done

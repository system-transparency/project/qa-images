variables:
  QA_STMGR_VERSION: v0.4.1
  QA_STIMAGES_GIT: https://git.glasklar.is/system-transparency/core/stimages.git
  QA_STIMAGES_VERSION: stimages-0.1.1
  QA_ROOTCERT: 'https://git.glasklar.is/glasklar/trust/glasklar-ca/-/raw/main/QA-CA/ST-signing/Glasklar%20QA%20ST%20signing%20root%20cert.pem'

stages:
  - stimages
  - kernels

# Build Debian bookworm OS image; publish if we're on a tag
qa-debian-bookworm:
  stage: stimages
  tags:
    - longrunning
  image: debian:bookworm
  variables:
    TARGET: ${CI_JOB_NAME}-amd64
  before_script:
    - apt-get -qq update
    - apt-get install -qqy curl git golang-1.19-go pigz
    - apt-get install -qqy make mmdebstrap libsystemd-shared cpio
    - |
      export GOPATH="${PWD}"/.go; mkdir -p "${GOPATH}"
      export PATH="${PATH}":/usr/lib/go-1.19/bin:"${GOPATH}"/bin
      go install system-transparency.org/stmgr@"${QA_STMGR_VERSION}"
  script:
    - git clone -b "${QA_STIMAGES_VERSION}" "${QA_STIMAGES_GIT}"
    - cd stimages
    - cp -a ../config/bookworm config/
    - make SIGN= CONFIG=config/bookworm BINDIST="${TARGET}" STIMAGE_NAME="${TARGET}" NETBOOT_URL="https://st.glasklar.is/st/qa/${TARGET}" stimage
    - test -z "$CI_COMMIT_TAG" && exit 0
    - ../publish.sh "build/${TARGET}" .json .zip
  artifacts:
    paths:
      - stimages/build/${TARGET}.json
      - stimages/build/${TARGET}.zip

# Build stboot images; publish if we're on a tag
qa-stboot:
  stage: stimages
  image: debian:bookworm
  variables:
    TARGET: ${CI_JOB_NAME}-amd64
  before_script:
    - apt-get -qq update
    - apt-get install -qqy curl git golang-1.19-go pigz make cpio
    - |
      export GOPATH="${PWD}"/.go; mkdir -p "${GOPATH}"
      export PATH="${PATH}":/usr/lib/go-1.19/bin:"${GOPATH}"/bin
  script:
    - git clone -b "${QA_STIMAGES_VERSION}" "${QA_STIMAGES_GIT}"
    - cd stimages
    - mkdir keys
    - curl -so keys/rootcert.pem "$QA_ROOTCERT"
    - make NETBOOT_URL="https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64" stboot-iso stboot-uki
    - test -z "$CI_COMMIT_TAG" && exit 0
    - cp build/stboot.iso "${TARGET}.iso"
    - cp build/stboot.uki "${TARGET}.uki"
    - ../publish.sh "${TARGET}" .iso .uki
  artifacts:
    paths:
      - stimages/build/stboot.uki
      - stimages/build/stboot.iso

# Publish stboot kernel from latest qa-debian-bookworm in package registry
qa-stboot-kernel:
  stage: kernels
  only:
    - tags
  image: debian:bookworm
  variables:
    TARGET: ${CI_JOB_NAME}-amd64
  script:
    - apt-get -qq update
    - apt-get install -qqy curl unzip
    - curl -sO "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/qa-debian-bookworm/${CI_COMMIT_TAG}/qa-debian-bookworm-amd64.zip"
    - unzip -p qa-debian-bookworm-amd64.zip boot/qa-debian-bookworm-amd64.vmlinuz > "${TARGET}.vmlinuz"
    - ./publish.sh "${TARGET}" .vmlinuz
